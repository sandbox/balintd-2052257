
-- SUMMARY --

The Flickr Favorites module provides a block to your Drupal site with favorited Flickr images of a Flickr user in it.

-- REQUIREMENTS --

* Libraries API https://drupal.org/project/libraries

* phpflickr 3.1 API Kit https://code.google.com/p/phpflickr/


-- INSTALLATION --

* Install the module as usual.

* Copy the phpflickr 3.1 API kit to your sites/all/libraries folder (for example: sites\all\libraries\phpFlickr-3.1)


-- CONFIGURATION --

Once you've enabled the Flickr Favorites module, it'll create a block, you can configure this block based on your
needs under Structure -> Blocks -> Flickr Favorites (admin/structure/block/manage/flickr_favs/flickr_favs/configure)

* Enter your Flickr API Key to the 'Flickr API Key' field. If you don't have one, read below for the instructions
  
  In order for the Flickr Favorites module to be able to connect to Flickr,
  you'll need to create a Flickr App and get your Flickr API Key

  - Log in to Flickr and go to The App Garden page http://www.flickr.com/services/

  - Under 'Your Apps' select 'Get an API Key' and select what kind of API Key do you need for your site

  - Fill out the form with informations about your website and after you've submitted it, you'll get your API Key.
    Copy and paste that into the 'Flickr API Key' field on the Flickr Favorites block's configuration page.

* Enter the Flickr user's username to the 'Flickr username' field.

* Select the number of photos to appear in the block.


-- CUSTOMIZATION --

* If you wish to customize the HTML output of the block, you can by copying the 'flickr_favs.tpl.php' over your
  theme's 'templates' folder and making your changes.


-- CONTACT --

Current maintainer:

Bálint Deáki - http://drupal.org/user/1026334


This project has been sponsored by:

Internet Simplicity
-------------------
We are a Silicon Valley web development firm with a passion for the 
technical end of website development.

http://www.internetsimplicity.net/