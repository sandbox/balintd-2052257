<?php
/**
 * @file
 * The template that controls the HTML output of the images
 */
?>

<ul class="flickr-favorites">
  <?php foreach ($images as $image): ?>
  	<li class="flickr-favorite">
  	  <img src="<?php print $image; ?>">
  	</li>
  <?php endforeach; ?>
</ul>